/*
* MAS514
* Ex4 - Problem 1b 
* Integration Example
*/
float dt,t,t_1;
float Position = 0; // m
float Velocity = 1; // m/s

void setup() {
  /* Serial to display data */
  Serial.begin(19200);
}

void loop() {
  // Integrator time  
    t_1 = t;                 // Previous time is stored before the actual time read
    t = millis();            // Current time actual time read
    dt = (t - t_1) / 1000;   // Divide by 1000 to get seconds  
    // Estimate Position 
    Position = Position + Velocity*dt;
    // Serial Pring
    Serial.print("t [s]: ");
    Serial.print(t/1000, 2);
    Serial.print(",");
    Serial.print("dt: ");
    Serial.print(dt, 4);
    Serial.print(",");
    Serial.print("Position: ");
    Serial.print(Position, 2);
    Serial.print(",");
    Serial.print("Velocity: ");
    Serial.println(Velocity, 2);
}